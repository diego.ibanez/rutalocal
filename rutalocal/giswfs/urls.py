from django.urls import path
from . import views

urlpatterns = [
    path("ot/", views.OfertaTuristicaWFSView.as_view()),
]