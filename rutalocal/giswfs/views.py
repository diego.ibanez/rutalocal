from django.shortcuts import render
from gisserver.features import FeatureType, ServiceDescription
from gisserver.geometries import CRS, WGS84
from gisserver.views import WFSView
from plataforma.models import OfertaTuristica

RD_NEW = CRS.from_srid(28992)


class OfertaTuristicaWFSView(WFSView):
    """An simple view that uses the WFSView against our test model."""

    xml_namespace = "https://rutalocal.nestolab.cl/gisserver"

    # The service metadata
    service_description = ServiceDescription(
        title="OT",
        abstract="Unittesting",
        keywords=["django-gisserver"],
        provider_name="Django",
        provider_site="https://rutalocal.nestolab.cl/",
        contact_person="django-gisserver",
    )

    # Each Django model is listed here as a feature.
    #print(OfertaTuristica.objects.all())
    feature_types = [
        FeatureType(
            OfertaTuristica.objects.all(),
            fields=["id","tipo_oferta","nombre","lugar"],
            other_crs=[RD_NEW]
        ),
    ]