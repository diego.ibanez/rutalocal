from django.urls import path
from . import views
from djgeojson.views import GeoJSONLayerView
from .models import OfertaTuristica
from django.conf.urls import url
urlpatterns = [
    path('v0/', views.HomeView.as_view(), name='home'),
    path('', views.HomeViewV2.as_view(), name='home'),
    path('crearzona/', views.ZonaCreateView.as_view(), name='crearzona'),
    path('crear/<int:tipo>', views.OfertaTuristicaCreateView.as_view(), name='crearoferta'),
    path('crearpunto/', views.OfertaTuristicaCreateView.as_view(), name='crearpunto'),
    path('mispuntos/', views.HomeView.as_view(), name='mispuntos'),
    path(r'^data.geojson$', GeoJSONLayerView.as_view(model=OfertaTuristica, properties=('tipo_oferta', 'nombre', 'lugar','descripcion','id'),geometry_field='lugar' ), name='data'),
]