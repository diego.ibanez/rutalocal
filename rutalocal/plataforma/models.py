from django.db import models
from django.contrib.gis.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.validators import MinLengthValidator

# Create your models here.

class Comuna(models.Model):
    name = models.CharField(max_length=255)
    cod = models.CharField(max_length=255)
    region = models.CharField(max_length=255)
    cod_region = models.CharField(max_length=255)
    av_region = models.CharField(max_length=255)
    cod_provincia = models.CharField(max_length=255)
    provincia = models.CharField(max_length=255)
    nse = models.CharField(max_length=255)
    tamano = models.CharField(max_length=255)
    tipo = models.CharField(max_length=255)



    '''
    ACA VAMOS AGREGAR EL RESTO DE CAMPOS
    '''
    def get_comuna_choices():
        try:
            comuna_choices = [(p['cod'], p['name']) for p in self.objects.values('cod','name').distinct()]
            return comuna_choices
        except:
            return []
    def get_region_choices():
        try:
            region_choices = [(p['cod_region'], p['region']) for p in self.objects.values('cod_region','region').distinct()]
            return region_choices
        except:
            return []
    def get_comuna_autocomplet_dict():
        try:
            communes_autocomplet_dict = {l.lower().replace(' ','_').replace('ñ','n').replace('á','a').replace('é','e').replace('í','i').replace('ó','o').replace('ú','u').strip() :l for l in self.objects.order_by('name').values_list('name',flat=True) if l.lower().replace(' ','').replace('ñ','n').replace('á','a').replace('é','e').replace('í','i').replace('ó','o').replace('ú','u').strip().isalpha()}
            return communes_autocomplet_dict
        except:
            return {}

    def __str__(self):
        return self.name

class Ruta(models.Model):
#    comuna = models.ForeignKey(Comuna, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=255)
    descripcion=models.TextField()
    centro = models.PointField()
    zoom = models.IntegerField(default=10)
    poly = models.PolygonField()
    prioriodad = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True,help_text=("creation date"))
    def __str__(self):
        return f"{self.nombre}"# - {self.comuna}"
    class Meta:
        ordering = ('-prioriodad',)


class Localidad(models.Model):
#    comuna = models.ForeignKey(Comuna, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=255)
    descripcion=models.TextField()
    centro = models.PointField()
    zoom = models.IntegerField(default=10)
    poly = models.PolygonField()
    prioriodad = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True,help_text=("creation date"))
    def __str__(self):
        return f"{self.nombre}"# - {self.comuna}"
    class Meta:
        ordering = ('-prioriodad',)


class Zona(models.Model):
#    comuna = models.ForeignKey(Comuna, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=255)
    descripcion=models.TextField()
    centro = models.PointField()
    zoom = models.IntegerField(default=10)

    prioriodad = models.IntegerField(default=0)

    created_at = models.DateTimeField(auto_now_add=True,help_text=("creation date"))
    def __str__(self):
        return f"{self.nombre}"# - {self.comuna}"
    class Meta:
        ordering = ('-prioriodad',)

CH = range(10)

TIPO_OFERTA_CHOICES = [
        (CH[0], 'Alojamiento'),
        (CH[1], 'Guía cultural'),
        (CH[2], 'Actividades al aire libre'),
        (CH[3], 'Actividades de interior'),
        (CH[4], 'Alimentación'),
        (CH[5], 'Servicios'),
        (CH[6], 'Transporte'),
        (CH[7], 'Historia local'),
    ]
    
TIPO_OFERTA_ICON = {
        CH[0]: 'icons/hostal.png',
        CH[1]: 'icons/hostal.png',
        CH[2]: 'icons/hostal.png',
        CH[3]: 'icons/hostal.png',
        CH[4]: 'icons/cutlery.png',
        CH[5]: 'icons/hostal.png',
        CH[6]: 'icons/hostal.png',
        CH[7]: 'icons/hostal.png',
    }

class OfertaTuristica(models.Model):
#    comuna = models.ForeignKey(Comuna, on_delete=models.CASCADE)
    tipo_oferta =  models.IntegerField(
        choices=TIPO_OFERTA_CHOICES,
        default=CH[0],
    )
    nombre = models.CharField(max_length=255)
    descripcion=models.TextField()
    
    email = models.EmailField(verbose_name='Correo de contacto',max_length=50)
    phone = models.CharField(verbose_name='Fono de contacto',max_length=50)
    first_name = models.CharField(verbose_name= 'Nombre', max_length=30, blank=False, help_text=(u"El nombre de la persona a cargo"), validators=[MinLengthValidator(2)])
    second_name = models.CharField(verbose_name= 'Segundo nombre', max_length=30, blank=False, help_text=(u"El segundo nombre de la persona a cargo"), validators=[MinLengthValidator(2)])
    first_last_name = models.CharField(verbose_name= 'Apellido', max_length=30, blank=False, help_text=(u"El apellido de la persona a cargo"), validators=[MinLengthValidator(2)])
    second_last_name = models.CharField(verbose_name= 'Segundo apellido', max_length=30, blank=False, help_text=(u"El primer apellido de la persona a cargo"), validators=[MinLengthValidator(2)])

    instagram = models.CharField(max_length=255)

    area = models.PolygonField(blank=True, null=True)
    
    ruta = models.LineStringField(blank=True, null=True)

    lugar = models.PointField(blank=True, null=True)
    
    created_at = models.DateTimeField(auto_now_add=True,help_text=("creation date"))
    active = models.BooleanField(default=True)
    published = models.BooleanField(default=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.nombre} - {self.tipo_oferta}"
    def get_context_result(self):
        return self
    def dict_tipo_oferta():
        return {id:{'nombre':nombre, 'icon': TIPO_OFERTA_ICON[id]} for (id, nombre) in TIPO_OFERTA_CHOICES}
    class Meta:
        ordering = ('-created_at',)