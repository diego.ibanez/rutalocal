#from django import forms
from django.contrib.gis import forms

from .models import Zona, OfertaTuristica
from django.templatetags.static import static

from leaflet.forms.widgets import LeafletWidget


from django import forms
#from .models import Schema
  
# iterable
# schema_choices = Schema.objects.all().values_list('id', 'name')
# print(schema_choices)
schema_choices = []
# creating a form 
class ZonaForm(forms.Form):
    zona = forms.ChoiceField(choices=[],widget=forms.Select(attrs={'onchange': 'zonaform.submit();'}))
    #schema_choices = []

    def __init__(self, *args, **kwargs):
        super(ZonaForm, self).__init__(*args, **kwargs)

        #print(Schema.objects.all().values_list('id', 'name'))
        #self.schema = forms.ChoiceField(choices=[(obj.id, obj.name) for obj in Schema.objects.all()])
        self.fields['zona'].choices = [(obj.id, obj.nombre) for obj in Zona.objects.all()]
        
        
        #self.schema = forms.ChoiceField(choices=[(obj.id, obj.name) for obj in Schema.objects.all()])


#class OLWtunned(forms.OpenLayersWidget):
#    class Media:
#        #pass
#        js = (static('ol.js'),)



class OfertaTuristicaCreateForm(forms.ModelForm):
    class Meta:
        model = OfertaTuristica
        fields = ('nombre','descripcion','lugar','ruta','area','tipo_oferta')
        widgets = {'lugar': LeafletWidget(),'ruta': LeafletWidget(),'area': LeafletWidget()}#,'tipo_oferta':forms.HiddenInput()}

    def __init__(self, *args, **kwargs):
        to = kwargs.pop('tipo_oferta' , None)
        super(OfertaTuristicaCreateForm, self).__init__(*args, **kwargs)
        if to == 0:
            #        (CH[0], 'Alhojamiento'),
            self.fields['area'].widget = forms.HiddenInput()
            self.fields['ruta'].widget = forms.HiddenInput()

        elif to == 1:
            #        (CH[1], 'Guía cultural'),

            #self.fields['area'].widget = forms.HiddenInput()
            #self.fields['ruta'].widget = forms.HiddenInput()
            #self.fields['lugar'].widget = forms.HiddenInput()
            pass
        elif to == 2:
            #        (CH[6], 'Transporte'),
            #self.fields['area'].widget = forms.HiddenInput()
            #self.fields['ruta'].widget = forms.HiddenInput()
            self.fields['lugar'].widget = forms.HiddenInput()
        if to:
            self.fields['tipo_oferta'].initial = int(to)
        
        #(CH[1], 'Guía cultural'),
        #(CH[2], 'Actividades al aire libre'),
        #(CH[3], 'Actividades de interior'),
        #(CH[4], 'Alimentación'),
        #(CH[5], 'Servicios'),
        #(CH[6], 'Transporte'),
        #(CH[7], 'Historia local'),


class PuntoTuristicoCreateForm(forms.ModelForm):
    class Meta:
        model = OfertaTuristica
        fields = ('nombre','descripcion','lugar','ruta','area','tipo_oferta')
        widgets = {'lugar': LeafletWidget(),'ruta': LeafletWidget(),'area': LeafletWidget()}#,'tipo_oferta':forms.HiddenInput()}

class ZonaCreateForm(forms.ModelForm):
    class Meta:
        model = Zona
        fields = ('nombre','descripcion','centro','zoom')
        widgets = {'centro': LeafletWidget()}

class FiltroMapa(forms.Form):
    zona = forms.ChoiceField(choices=[])
    capa = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,choices=[])
    #schema_choices = []
    def __init__(self, *args, **kwargs):
        super(FiltroMapa, self).__init__(*args, **kwargs)
        #print(Schema.objects.all().values_list('id', 'name'))
        #self.schema = forms.ChoiceField(choices=[(obj.id, obj.name) for obj in Schema.objects.all()])
        self.fields['zona'].choices = [(obj.id, obj.name) for obj in Zona.objects.all()]
        self.fields['capa'].choices = [(obj.id, obj.name) for obj in Schema.objects.all()]

        #self.schema = forms.ChoiceField(choices=[(obj.id, obj.name) for obj in Schema.objects.all()])
