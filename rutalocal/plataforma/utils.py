#from base.enums import SensorKind
from OSMPythonTools.nominatim import Nominatim
from OSMPythonTools.overpass import overpassQueryBuilder, Overpass
import geopandas as gpd
import pandas as pd
import numpy as np

#from base.enums import TerritorioKind as TK
#from base.enums import Dimensiones as DM
#
#from base.enums import SensorKind as SK
#from base.enums import FieldKind as FK
#from base.enums import get_name_fields
#from puerto.models import IndicadoresSIEDU, MetadatosIndicadoresSIEDU
#
from bokeh.models.widgets import DataTable, DateFormatter, TableColumn
from bokeh.models import ColumnDataSource

#from api.models import City
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.layouts import row,column
from bokeh.resources import CDN
from bokeh.tile_providers import CARTODBPOSITRON, get_provider, OSM, ESRI_IMAGERY
from bokeh.tile_providers import get_provider, Vendors
from pyproj import Proj, transform
from bokeh.models import  GeoJSONDataSource,HoverTool,CustomJS
import geojson

#from base.constants import dict_pobr_multi, dict_pobr_multi_pond, color_pobrezam,color_ambito_pobrezam
#from puerto.models import Casen_2017

from bokeh.io import output_file, show
from bokeh.models import ColumnDataSource
from bokeh.palettes import GnBu3, OrRd3, Oranges
from bokeh.plotting import figure

#from base.enums import get_fields, get_name_fields, FieldKind
# output (meters east of 0, meters north of 0): (-14314.651244750548, 6711665.883938471)
from django.templatetags.static import static

from puerto.models import Censo_2017_UV, Censo_2017_Manzana, Casen_2017,RegistroSensor
from django.contrib.gis.db.models.functions import Centroid, Transform
from shapely import wkt, wkb
import geopandas as gp
from bokeh.palettes import brewer
from bokeh.models import (CDSView, ColorBar, ColumnDataSource,
                          CustomJS, CustomJSFilter,
                          GeoJSONDataSource, HoverTool,
                          LinearColorMapper, Slider,
                          Div)
import ee
from datetime import datetime, date
from .models import Zona



def settings_openlayers(zona_id):
    if zona_id == None:
        zona_id = Zona.objects.all().first().id
    zona_model = Zona.objects.get(id=zona_id)
    ee.Initialize()
    CLOUD_FILTER = 1
    dataset = (ee.ImageCollection("COPERNICUS/S2_SR")
                      .filter(ee.Filter.date('2021-01-01', str(date.today())))
                      .filter(ee.Filter.lte('CLOUDY_PIXEL_PERCENTAGE', CLOUD_FILTER))
                      .mosaic()
                      )
                      #.mosaic())
                      #.first())
    #modisrgb = dataset.select('TCI_R','TCI_G','TCI_B')
    #Styling 
    vis_paramsRGB = {
      #"bands": ["TCI_R","TCI_B","TCI_G",]
      "bands": ["B4", "B3", "B2"],
      "gain": '0.08, 0.08, 0.08',
      #"scale": 1
    }
    #add the map to the the folium map
    map_id_dict = dataset.getMapId(vis_paramsRGB)
    tile_url = map_id_dict['tile_fetcher'].url_format
    #print(tile_url)
    
    OPENLAYERS_CONFIG = {
        # conf here
        #'SPATIAL_EXTENT': (5.0, -38.7999968, 7.5, -71.2833322),
        'DEFAULT_CENTER': (zona_model.centro[1], zona_model.centro[0]),
        'CENTER': [zona_model.centro[0],zona_model.centro[1]],
        'ZOOM': zona_model.zoom,
        'MIN_ZOOM': 6,
        'MAX_ZOOM': 15,
        'DEFAULT_PRECISION': 7,
        'ATTRIBUTION_PREFIX':'<a href="https://django-leaflet.readthedocs.io/en/latest/" target="_blank">Django-Leaflet</a>',
        'TILES': [('GEE Sentinel-2', tile_url,{'attribution': '<a href="https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S2_SR" target="_blank">EE Sentinel-2</a>'})],
        'TILE':tile_url,
    }

    return OPENLAYERS_CONFIG


def settings_leafletmap(zona_id):
    if zona_id == None:
        zona_id = Zona.objects.all().first().id
    zona_model = Zona.objects.get(id=zona_id)
    ee.Initialize()
    CLOUD_FILTER = 1
    dataset = (ee.ImageCollection("COPERNICUS/S2_SR")
                      .filter(ee.Filter.date('2021-01-01', str(date.today())))
                      .filter(ee.Filter.lte('CLOUDY_PIXEL_PERCENTAGE', CLOUD_FILTER))
                      .mosaic()
                      )
                      #.mosaic())
                      #.first())
    #modisrgb = dataset.select('TCI_R','TCI_G','TCI_B')
    #Styling 
    vis_paramsRGB = {
      #"bands": ["TCI_R","TCI_B","TCI_G",]
      "bands": ["B4", "B3", "B2"],
      "gain": '0.08, 0.08, 0.08',
      #"scale": 1
    }
    #add the map to the the folium map
    map_id_dict = dataset.getMapId(vis_paramsRGB)
    tile_url = map_id_dict['tile_fetcher'].url_format
    #print(tile_url)
    
    LEAFLET_CONFIG = {
        # conf here
        #'SPATIAL_EXTENT': (5.0, -38.7999968, 7.5, -71.2833322),
        'DEFAULT_CENTER': (zona_model.centro[1], zona_model.centro[0]),
        'DEFAULT_ZOOM': zona_model.zoom,
        'MIN_ZOOM': 6,
        'MAX_ZOOM': 15,
        'DEFAULT_PRECISION': 7,
        'ATTRIBUTION_PREFIX':'<a href="https://django-leaflet.readthedocs.io/en/latest/" target="_blank">Django-Leaflet</a>',
        'TILES': [('GEE Sentinel-2', tile_url,{'attribution': '<a href="https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S2_SR" target="_blank">EE Sentinel-2</a>'})],
    }

    return LEAFLET_CONFIG

def create_geodataframe(list_result):
    d = {}
    for n, r in enumerate(list_result):
        d_aux = {}
        d_aux.update(r.tags())
        d_aux.update({'lat':r.lat(),'lon':r.lon()})
        d.update({n:d_aux})
    df = pd.DataFrame.from_dict(d,orient="index").drop_duplicates(subset=None, keep='first', inplace=False)
    gdf = gpd.GeoDataFrame(df, geometry=gpd.points_from_xy(df.lon, df.lat)).set_crs(epsg=4326).to_crs(epsg=3857) ## 4326 OSM format
    return gdf

def get_OSM_points(location='Antofagasta, Chile', selectors_dict= {'amenities':'"amenity"','shops':'"shop"','tourism':'"tourism"','trees':'"natural"="tree"'}):
    nominatim = Nominatim()
    areaId = nominatim.query(location).areaId()
    results = {}

    overpass = Overpass()

    for k, s in selectors_dict.items():
        query = overpassQueryBuilder(area=areaId,includeGeometry=True, elementType='node',selector=s)#, selector='"natural"="tree"')#, out='body')
        #query1 = overpass._queryString(query,timeout=200)
        result = overpass.query(query)#,onlyCached=True)#,timeout=1000000)
        results.update({k:create_geodataframe(result.elements())})

    return results

def plot_dimension(name,id_, min_, max_, I, II,III,value,rep_form, First=True):
    indicador = [id_]
    q = 'Cuartiles'

    ind = {'indicador': [id_,id_],#,q,q,q,q],
            'left':[min_, value],#,min_,I,II,III],
            'right':[value,max_],#,I,II,III,max_],
            'colors':['#ff6600','#000000'],#,'Orange','Yellow','Green','Purple'],
            'values':[value,value],#,value#,value,value,value],
            'min':[min_,min_],#,min_#,min_,min_,min_],
            'I':[I,I],#,I,I,I,I],
            'II':[II,II],#,II#,II,II,II],
            'III':[III,III],#,III#,III,III,III],
            'max':[max_,max_],#,max_#,max_,max_,max_],
            'legends':['de mínimo nacional a valor comunal','de valor comunal a máximo nacional'],#,'25% nacional','50% nacional (mediana)','%75 nacional'],
            'rep_form' : [rep_form, rep_form],#,rep_form, rep_form,rep_form, rep_form]
}

    margen = (ind["max"][0] - ind["min"][0])/5
    p = figure(y_range=list(set(ind['indicador'])), x_range=(ind["min"][0] - margen, ind["max"][0] + margen), title=name,
               toolbar_location=None,
               plot_height=50,
               sizing_mode='stretch_width')
#,plot_width=500,plot_height=300)
#q
    source = ColumnDataSource(data=ind)

    p.hbar(y='indicador',height=1,left='left',right='right',fill_color="colors", legend='legends', source=source, line_width=0)

    #p.text(y=[ind['indicador'][0]], x=[ind['values'][0]],text=['Antofagasta: {}'.format(ind['values'][0])],
    #        text_font_size="15px", text_align="left", text_baseline="middle", text_color="white")
#
    #p.text(y=[ind['indicador'][0]], x=[ind['left'][0]],text=['{}'.format(ind['left'][0])],
    #        text_font_size="15px", text_align="right", text_baseline="middle",text_font_style="bold", text_color="black")
#
    #p.text(y=[ind['indicador'][0]], x=[ind['right'][1]],text=['{}'.format(ind['right'][1])],
    #        text_font_size="15px", text_align="right", text_baseline="middle",text_font_style="bold", text_color="white")

    p.add_tools(HoverTool(tooltips=[('Unidad','@rep_form'),("comunal", "@values"),("max nacional","@max"),("75% nacional", "@III"),("50% nacional", "@II"),("25% nacional", "@I"), ("min nacional", "@min"),]))
    p.yaxis.visible = False
    p.xaxis.visible = False
    #p.background.visible = False
    p.background_fill_alpha = 0
    p.title.text_font_size = '7.5pt'
    p.title.visible = False
    p.y_range.range_padding = 0.1
    p.ygrid.grid_line_color = None
    p.legend.location = "top_left"
    #if not First:
    p.legend.visible = False
    p.axis.minor_tick_line_color = None
    p.outline_line_color = None
    p.background_fill_alpha = 0
    p.grid.grid_line_color = None

    ##
    pt = figure(y_range=list(set(ind['indicador'])), x_range=(ind["min"][0] - margen, ind["max"][0] + margen), title=name,
               toolbar_location=None,
               plot_height=40,
               sizing_mode='stretch_width')
#,plot_width=500,plot_height=300)
#q
    #source = ColumnDataSource(data=ind)
    pt.text(y=[ind['indicador'][0]], x=[ind['values'][0]],text=['Antofagasta: {}'.format(ind['values'][0])],
            text_font_size="15px", text_align="left", text_color="black")# text_baseline="middle"

    pt.text(y=[ind['indicador'][0]], x=[ind['left'][0]],text=['mín: {}'.format(ind['left'][0])],
            text_font_size="15px", text_align="right", text_color="black")#text_font_style="bold", #text_baseline="hanging",

    pt.text(y=[ind['indicador'][0]], x=[ind['right'][1]],text=['máx: {}'.format(ind['right'][1])],
            text_font_size="15px", text_align="left", text_color="black")#,text_font_style="bold" text_baseline="hanging"
    pt.yaxis.visible = False
    pt.xaxis.visible = False
    #pt.background.visible = False
    pt.background_fill_alpha = 0
    pt.title.visible = False
    pt.title.text_font_size = '7.5pt'

    pt.y_range.range_padding = 0.1
    pt.ygrid.grid_line_color = None
    pt.legend.location = "top_left"
    #ift not First:
    pt.legend.visible = False
    pt.axis.minor_tick_line_color = None
    pt.outline_line_color = None
    pt.background_fill_alpha = 0
    pt.grid.grid_line_color = None

    ##

    full_description = MetadatosIndicadoresSIEDU.objects.filter(ID=id_).first()

    title = full_description.Indicador_Titulo_del_recurso
    measure_type = full_description.Unidad_Forma_de_representacion # Unidad_Forma_de_representacion # Fecha_de_creacion
    description = full_description.Descripcion_Resumen
    fecha= full_description.Fecha_de_actualizacion.split(" ")[0]
    fuente = "SIEDU"
    div_text = Div(text="""<h1><b>{}</b> ({})</h1><br>Fuente: {} {}<br><br>{}""".format(title, measure_type,fuente,fecha,description),sizing_mode="stretch_width")

    ### blocking movimng
    pt.toolbar.active_drag = None
    pt.toolbar.active_scroll = None
    pt.toolbar.active_tap = None
    p.toolbar.active_drag = None
    p.toolbar.active_scroll = None
    p.toolbar.active_tap = None

    return column(div_text,p,pt,sizing_mode='stretch_width')

def get_dimension_view(dimension, CUT=2101):
    values_dimension_pre = DM.DICT_DIMENSIONES_INDICADORES_SIEDU[dimension]
    field_names = [f.name for f in IndicadoresSIEDU._meta.fields]
    values_dimension = [d for d in values_dimension_pre if d in field_names]

    values_dimension_pre_prio = DM.DICT_DIMENSIONES_INDICADORES_PRIORITAROS_SIEDU[dimension]
    values_dimension_plot = [d for d in values_dimension_pre_prio if d in field_names]
    values_dimension_table = [d for d in values_dimension if d not in values_dimension_plot]
    ## all values resumen
    all_ind = IndicadoresSIEDU.objects.all().values(*values_dimension)
    description_all = pd.DataFrame(list(all_ind)).describe().T
    description_all.columns = description_all.columns + '_nacional'

    ## get metadata
    metadatas = MetadatosIndicadoresSIEDU.objects.filter(ID__in=values_dimension).values()
    meta_df = pd.DataFrame(list(metadatas))

    indi = IndicadoresSIEDU.objects.filter(CUT=CUT).values(*values_dimension)
    df_indi = pd.DataFrame(list(indi)).T
    df_indi = df_indi.rename(columns={0:'valor_comunal'})
    df_full = df_indi.merge(meta_df,left_index=True,right_on='ID',how='inner').merge(description_all, left_on='ID', right_index=True, how='inner')


    df_full_plot = df_full[df_full['ID'].isin(values_dimension_plot)]
    df_full_table = df_full[df_full['ID'].isin(values_dimension_table)]

    #Columns = [TableColumn(field=Ci, title=Ci) for Ci in df_full_table.columns] # bokeh columns
    #data_table = DataTable(columns=Columns, source=ColumnDataSource(df_full_table)) # bokeh table
    data_table = df_full_table[['Indicador_Titulo_del_recurso','valor_comunal','Fecha_de_actualizacion','Descripcion_Resumen','Unidad_Forma_de_representacion','min_nacional', '25%_nacional', '50%_nacional','75%_nacional', 'max_nacional']].to_dict(orient='index')
    #'valor_comunal', 'ID', 'Compromiso', 'Atributo','Indicador_Titulo_del_recurso', 'Tipo_de_indicador','Ano_del_indicador', 'Escala_del_indicador', 'Descripcion_Resumen','Tipo_de_procesamiento', 'Cobertura_del_Indicador','N_de_unidades_territoriales', 'Unidad_Forma_de_representacion','Fecha_de_creacion', 'Fecha_de_actualizacion', 'Frecuencia','Palabras_claves', 'Categoria_del_indicador','Responsable_del_metadato', 'Limitaciones_del_Indicador','Relacion_con_otros_indicadores','Variable_1_requerida_para_la_construccion_del_indicador','Fuente_variable_1_Nombre_de_la_organizacion', 'Ano_variable_1','Escala_disponible_de_variable_1','Variable_2_requerida_para_la_construccion_del_indicador','Fuente_variable_2_Nombre_de_la_organizacion', 'Ano_variable_2','Escala_disponible_de_variable_2','Variable_3_requerida_para_la_construccion_del_indicador','Fuente_variable_3_Nombre_de_la_organizacion', 'Ano_variable_3','Escala_disponible_de_variable_3','Variable_4_requerida_para_la_construccion_del_indicador','Fuente_variable_4_Nombre_de_la_organizacion', 'Ano_variable_4','Escala_disponible_de_variable_4', 'count_nacional', 'mean_nacional','std_nacional', 'min_nacional', '25%_nacional', '50%_nacional','75%_nacional', 'max_nacional'
    #dimensiones = ['']
    ###
    ### CREATION OF PLOTS LIST
    plots_list = []
    for n, r in df_full_plot.iterrows():
        if n == 0:
            First = True
        else:
            First = False
        plots_list.append(plot_dimension(name=r['Indicador_Titulo_del_recurso'],id_=r['ID'],min_=r['min_nacional'],max_=r['max_nacional'],value=r['valor_comunal'],rep_form=r['Unidad_Forma_de_representacion'],I=r['25%_nacional'],II=r['50%_nacional'],III=r['75%_nacional'], First=First))
    ###
    ###
    return data_table, plots_list

def getCasenMultiPlot(CUT=2102):
        """ Multi-dimensional y por Ingresos (decíles)
            CUT COM ANTOFA 2101 """
        #cuts = [2101]
        casen_com = Casen_2017.objects.filter(comuna__in=[CUT])#.values(values_censo)
        #casen_com = Casen_2017.objects.all()
        values_casen = ['expc','pobreza_multi_5d']
        for k, d in dict_pobr_multi.items():
          values_casen.extend(list(d.keys()))
        df_p = pd.DataFrame(casen_com.values(*values_casen))

        df_p_n = df_p.loc[np.repeat(df_p.index.values, df_p['expc'].astype(int).values)].reset_index(drop=True)

        df_f = df_p_n.mean()
        dict_casen_pob = {}

        dict_valores = {}

        df_aux = pd.DataFrame()
        for k, d in dict_pobr_multi.items():
          d_aux = {'ambito':k}
          for k_, l in d.items():
            d_aux.update({k_:[df_f[k_]]})
            dict_valores.update({k_:{'Dimensión':k,'Población (%)':int(df_f[k_]*1000)/10, 'Indicador de carencia':l[17:]}})
          dict_casen_pob.update(d)
          df_aux = df_aux.append(pd.DataFrame.from_dict(d_aux))

        df = df_aux.reset_index(drop=True)


        ###
        ###


        width = 800
        height = 800
        inner_radius = 90
        outer_radius = 300 - 10

        minr = 0.3#sqrt(log(.001 * 1E4))
        maxr = 0#sqrt(log(1000 * 1E4))
        a = (outer_radius - inner_radius) / (minr - maxr)#(maxr-minr)#
        b = inner_radius - a * maxr

        def rad(mic):
            return a*mic + b#a * np.sqrt(np.log(mic * 1E4)) + b

        big_angle = 2.0 * np.pi / (len(df) + 1)
        small_angle = big_angle / 15

        p = figure(plot_width=width, plot_height=height, title="Pobreza Multidimensional - CASEN 2017",
            margin = (0,0,60,0),

            x_axis_type=None, y_axis_type=None,
            x_range=(-350, 350), y_range=(-350, 350),
            min_border=0, outline_line_color="black",
            background_fill_color="#fcf7e8")

        p.xgrid.grid_line_color = None
        p.ygrid.grid_line_color = None

        # annular wedges
        angles = np.pi/2 - big_angle/2 - df.index.to_series()*big_angle
        colors = [color_ambito_pobrezam[ambito] for ambito in df.ambito]
        p.annular_wedge(
            0, 0, inner_radius, outer_radius, -big_angle+angles, angles, color=colors,
        )


        # circular axes and lables
        labels = np.array([0,0.05,0.1,0.15,0.2,0.25,0.3])#np.power(10.0, np.arange(-3, 4))
        radii = a*labels + b#a * np.sqrt(np.log(labels * 1E4)) + b
        p.circle(0, 0, radius=radii, fill_color=None, line_color="#f0e1d2")
        p.text(0, radii[:-1], [str(int(r*100))+'%' for r in labels[:-1]],
               text_font_size="11px", text_align="center", text_baseline="middle")

#       ### title
        p.text(0, radii[-1]*1.07, ['Hogares carentes en:'],
               text_font_size="20px", text_align="center", text_baseline="middle",text_font_style="bold", text_color="black")
#
        ## radial axes
        p.annular_wedge(0, 0, inner_radius-10, outer_radius+10,
                        -big_angle+angles, -big_angle+angles, color="black")
#
        ## ambito labels
        xr = radii[-1]*np.cos(np.array(-big_angle/2 + angles))*1.05
        yr = radii[-1]*np.sin(np.array(-big_angle/2 + angles))*1.05
        label_angle=np.array(-big_angle/2+angles)
        label_angle += np.pi/2
        label_angle[label_angle < -np.pi/2] += np.pi # easier to read labels on the center side
        label_angle[0] += np.pi ## turning around Educación


        label_angle[label_angle < -np.pi/2] += np.pi
        p.text(xr, yr, df.ambito, angle=label_angle,
               text_font_size="16px", text_align="center", text_baseline="middle",text_font_style="bold", text_color="black")

        # small wedges
        # 'hh_d_asis',hh_d_rez','hh_d_esc',
        # 'hh_d_mal','hh_d_prevs','hh_d_acc'
        # 'hh_d_act','hh_d_cot','hh_d_jub',
        # 'hh_d_habitab','hh_d_hacina','hh_d_estado','hh_d_servbas','hh_d_entorno','hh_d_accesi','hh_d_medio',
        # 'hh_d_appart','hh_d_tsocial','hh_d_seg'
        dims = ['hh_d_asis','hh_d_rez','hh_d_esc','hh_d_mal','hh_d_prevs','hh_d_acc','hh_d_act','hh_d_cot','hh_d_jub','hh_d_habitab','hh_d_hacina','hh_d_estado','hh_d_servbas','hh_d_entorno','hh_d_accesi','hh_d_medio','hh_d_appart','hh_d_tsocial','hh_d_seg']
        codes = ['E3','E2','E1','S3','S2','S1','TS3','TS2','TS1','VE7','VE6','VE5','VE4','VE3','VE2','VE1','RC3','RC2','RC1']
        n_angs = [3,7,11,3,7,11,4,8,12,1,3,5,7,9,11,13,4,8,12]
        n_angs_tuple = [(3,5),(7,9),(11,13),(3,5),(7,9),(11,13),(3,5),(7,9),(11,13),(1,2),(3,4),(5,6),(7,8),(9,10),(11,12),(13,14),(3,5),(7,9),(11,13)]

        for n, dim in enumerate(dims):
            code = codes[n]
            n_ang = n_angs[n]
            n_ang_tuple = n_angs_tuple[n]


            p.annular_wedge(0, 0, inner_radius, rad(df[df[dim].isnull() == False][dim]),
                            -big_angle+angles[df[df[dim].isnull() == False].index[0]]+n_ang_tuple[0]*small_angle, -big_angle+angles[df[df[dim].isnull() == False].index[0]]+n_ang_tuple[1]*small_angle,
                            color=color_pobrezam[dim])

########    ##### text
            angle_ = -big_angle+angles[df[df[dim].isnull() == False].index[0]]+n_ang*small_angle
            x_ = radii[-1]*np.cos(np.array(angle_))*0.27
            y_ = radii[-1]*np.sin(np.array(angle_))*0.27
            label_angle=np.array([angle_])
            label_angle[label_angle < -np.pi/2] += np.pi # easier to read labels on the center side
            value_ = str(int(df[df[dim].isnull() == False][dim].tolist()[0]*100))+'%'
            p.text(x_, y_, [code], angle=label_angle[0],
                   text_font_size="14px",text_font_style='bold', text_align="center", text_baseline="middle", text_color='black')
            dict_valores[dim].update({'Código':code})
#############


#############
######## p1, FIGURA grafico registro social de hogares comuna vs region
#############

        df_dict = pd.DataFrame.from_dict(dict_valores, orient='index')
        Columns = [TableColumn(field=Ci, title=Ci) for Ci in df_dict.columns] # bokeh columns
        data_table = DataTable(columns=Columns, source=ColumnDataSource(df_dict)) # bokeh table


        return p, data_table

def create_dataview(territorio,dimension):

    if int(dimension) in list(DM.DICT_DIMENSIONES_INDICADORES_SIEDU.keys()):
        table_dimensions, plots_dimensions = get_dimension_view(dimension)
    else:
        table_dimensions = False

    if int(territorio) in list(TK.LAT_LON_TERRITORIO.keys()):
        LAT_LON = TK.LAT_LON_TERRITORIO[int(territorio)]
    else:
        return False, False

    lon_m , lat_m = transform(Proj(init='epsg:4326'), Proj(init='epsg:3857'), LAT_LON[1], LAT_LON[0])  # longitude first, latitude second.
    lon_m_delta = TK.DELTA_M[territorio][0]
    lat_m_delta = TK.DELTA_M[territorio][1]
    plot_height = TK.plot_size_hw[territorio][0]
    plot_width = TK.plot_size_hw[territorio][1]
    p = figure(
           #title = "Data view, Plenario",
           x_range=(lon_m - lon_m_delta, lon_m + lon_m_delta ), y_range=(lat_m - lat_m_delta, lat_m + lat_m_delta),
           #plot_height = plot_height, plot_width = plot_width,
           sizing_mode='stretch_both',

           toolbar_location = "below",
           tools = "pan, wheel_zoom,  reset",
           x_axis_type="mercator", y_axis_type="mercator")
    #p = figure(x_range=(-7836519.785745, -7833443.046065 ), y_range=(-2703768.783017,-2708183.021401 ),
    #               x_axis_type="mercator", y_axis_type="mercator")


    tile_provider = get_provider(OSM)
    p.add_tile(tile_provider)
    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = None
    p.xaxis.visible = False
    p.yaxis.visible = False

    callback = CustomJS(code="""
    getGraph(cb_obj.x, cb_obj.y)
    """
    )
    p.js_on_event('tap', callback)
    ##
    dfg = City.getPontinSensorGraph()

    geosource_sensor = GeoJSONDataSource( geojson=geojson.dumps(dfg))
    #Tenemos que cambiar los campos a desplegar.
    TOOLTIPS_sensor = [
            ('sensor', '@sensor_name'),
            ('temperatura', '@temperature'),
            ('humedad','@humidity'),
            ('presión','@pressure'),
            ('radiación','@radiation'),
            ('luminiscencia','@luxmeter'),
            ('ruido','@noise'),
            ('particulado 10','@particle10'),
            ('particulado 25','@particle25'),
            ('fecha', '@created_at')

        ]

    circulos = p.circle_dot(x='x', y='y', size=15, color='White', alpha=0.9, fill_color='blue', source=geosource_sensor)
    p.add_tools(HoverTool(renderers = [circulos], tooltips =TOOLTIPS_sensor))

    ##

    ##


    if table_dimensions:
        if dimension == 7:
            fig_casenmulti, table_dict = getCasenMultiPlot()
            script, div = components(column(p,fig_casenmulti, table_dict,*plots_dimensions), CDN)

        else:
            script, div = components(column(p,*plots_dimensions), CDN)

    else:
        script, div = components(column(p), CDN)
    return script, div, table_dimensions



def create_dataview_map(territorio,censo_data=False):

    if int(territorio) in list(TK.LAT_LON_TERRITORIO.keys()):
        LAT_LON = TK.LAT_LON_TERRITORIO[int(territorio)]
    else:
        return False, False

    lon_m , lat_m = transform(Proj(init='epsg:4326'), Proj(init='epsg:3857'), LAT_LON[1], LAT_LON[0])  # longitude first, latitude second.
    lon_m_delta = TK.DELTA_M[territorio][0]
    lat_m_delta = TK.DELTA_M[territorio][1]
    #lon_m_delta = lat_m_delta = 1000
    plot_height = TK.plot_size_hw[territorio][0]
    plot_width = TK.plot_size_hw[territorio][1]
    p = figure(
           x_range=(lon_m - lon_m_delta, lon_m + lon_m_delta ), y_range=(lat_m - lat_m_delta, lat_m + lat_m_delta),
           toolbar_location = "below",
           tools = "pan, wheel_zoom,  reset",
           x_axis_type="mercator", y_axis_type="mercator",
    )
     #      responsive=True)
    #p = figure(x_range=(-7836519.785745, -7833443.046065 ), y_range=(-2703768.783017,-2708183.021401 ),
    #               x_axis_type="mercator", y_axis_type="mercator")
    p.outline_line_color = None
    if censo_data:
        p = getCensoLayer(p=p)

    tile_provider = get_provider(OSM)
    p.add_tile(tile_provider)
    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = None
    p.xaxis.visible = False
    p.yaxis.visible = False

    ##
    dfg_dict = City.getPontinSensorGraph()
    for t, dfg in dfg_dict.items(): ## creating multi tooltips for diferent type of sensores
        if dfg is not None:
            geosource_sensor = GeoJSONDataSource( geojson=geojson.dumps(dfg))
            #tooltip_list = [(FK.DICT_CHOICES[k],'@'+k) for k in get_name_fields(t)]
            not_list = ['nombre','descripcion','sensor_name','kind','geometry','direccion','geometry_m','created_at','url','group']
            tooltip_list = [(FK.DICT_CHOICES[k],'@'+k+'{0.00}') for k in dfg.columns if (pd.isna(dfg[k].iloc[0]) is False) and k not in not_list ]



            TOOLTIPS_sensor = [('sensor', '@sensor_name')]+tooltip_list+[('fecha', '@created_at')]
            circulos = p.circle_dot(x='x', y='y', size=30, color='Green', alpha=0.3, fill_color='Green', source=geosource_sensor)
            images = p.image_url(url='url',x='x', y='y',h=50,w=50, source=geosource_sensor,anchor="center")
            p.add_tools(HoverTool(renderers = [circulos], tooltips =TOOLTIPS_sensor))





    return p


def getCensoLayer(p, comunas= ['ANTOFAGASTA']):
        censo = Censo_2017_Manzana.objects.filter(COMUNA__in=comunas)#.values(values_censo)
        censo = censo.annotate(geometry_m=Transform('geometry',3857))
        values_censo = ['TOTAL_PERSONAS','TOTAL_VIVIENDAS','geometry_m']

        df_p = pd.DataFrame(censo.values(*values_censo))
        #df_p['TOTAL_VIVIENDAS'] = df_p['TOTAL_VIVIENDAS'].replace([0],1)
        df_p['TOTAL_VIVIENDAS'] = df_p['TOTAL_VIVIENDAS'].replace([0],1)
        df_p['DEN'] = df_p['TOTAL_PERSONAS']/df_p['TOTAL_VIVIENDAS']
        df_p['LOG_DEN'] = np.log(df_p['DEN'] +1 )

        df_p['geometry_m'] = df_p['geometry_m'].apply(lambda x: str(x).split(";")[-1])
        df_p['geometry_m'] = df_p['geometry_m'].apply(wkt.loads)

        dfg = gp.GeoDataFrame(df_p, geometry= 'geometry_m')
        #g_p['geometry'] = g_p['geometry'].set_crs(epsg=900913)

        #censo = censo.transform(900913, field_name='geometry')
        #serialization = g_p.to_json()#serialize('geojson', censo, geometry_field='geometry_m')
        serialization =geojson.dumps(dfg)

        geosource = GeoJSONDataSource(geojson = serialization)
        ### MAP IN THE BACK: OSM, ESRI_IMAGERY, CARTODBPOSITRON

        palette = brewer['Oranges'][4]
        palette = palette[::-1] # reverse order of colors so higher values have darker colors
        # Instantiate LinearColorMapper that linearly maps numbers in a range, into a sequence of colors.
        color_mapper = LinearColorMapper(palette = palette, low =0, high = 6)
        manzanas = p.patches('xs','ys', source = geosource,
                           fill_color = {'field' :'DEN',
                                 'transform' : color_mapper},
                           line_color = 'gray',
                           line_width = 0.25,
                           fill_alpha = 1
                           )
        # Create hover too
        p.add_tools(HoverTool(renderers = [manzanas],
                              tooltips = [('Total viviendas','@TOTAL_VIVIENDAS'),
                                        ('Population','@TOTAL_PERSONAS'),('Den','@DEN')]
                                        )
                              )
        return p



def create_dataview_dim(territorio,dimension):
    """
        function to create de dimension's view
            """
    if int(dimension) in list(DM.DICT_DIMENSIONES_INDICADORES_SIEDU.keys()):
        table_dimensions, plots_dimensions = get_dimension_view(dimension)
    else:
        table_dimensions = False

    if dimension == 7:
        fig_casenmulti, table_dict = getCasenMultiPlot()
        script, div = components(column(fig_casenmulti, table_dict,*plots_dimensions,sizing_mode='stretch_width'), CDN)
    else:
        script, div = components(column(*plots_dimensions,sizing_mode='stretch_width'), CDN)
    return script, div, table_dimensions

def get_variables_view_dict(sensor):
    """creation of variables buttons
        {<field-name>: {'img': <fied-img>. 'value':<field-value>, 'tag' : <tag-sensorid-field>}   """
    if len(sensor) > 1:

        fields = [f for s in sensor for f in get_fields(s.kind)]
        kind = 4 # multi
        nombre = [s.nombre for s in sensor]#' - '.join(sensor.values_list('nombre', flat=True))
        tipo_ubicacion = [s.get_tipo_ubicacion_display() for s in sensor]#(sensor.values_list('tipo_ubicacion', flat=True))
        orientacion = [s.get_orientacion_display( ) for s in sensor] #(sensor.values_list('orientacion', flat=True))

#        tag_names = {f: s.nombre for s in sensor for f in get_fields(s.kind)}

        #for s in sensor:
        #    nombre += s.nombre

    elif len(sensor) == 1:
        #print(sensor,type(sensor))
        id_ = sensor.first().id
        kind = sensor.first().kind
        nombre = [sensor.first().nombre]
        tipo_ubicacion = [sensor.first().get_tipo_ubicacion_display()]
        orientacion = [sensor.first().get_orientacion_display()]

        fields = get_fields(kind)
    else:
        return {}

    #    last = City.objects.filter(sensor_name=nombre).order_by('-id')[0]
    tag_names = {f: s.nombre for s in sensor for f in get_fields(s.kind)}
    for s in sensor:
        last = City.objects.filter(sensor_name=s.nombre).order_by('-id').first()
        if last is not None:
            break
    dict_ = {'sensor':{'name':nombre,'img':static(SensorKind.IMG[kind]['BLA']),'datetime':getattr(last,'created_at'),'type':SensorKind.KIND_CHOICES_DICT[kind], 'tipo_ubicacion': tipo_ubicacion, 'orientacion': orientacion},'fields':{}}

    for f in fields:
        v = FieldKind.KIND_CHOICES_DICT[f]
        u = FieldKind.UNITY_DICT[f]
        last_n = City.objects.filter(**{v+'__isnull':False},sensor_name=tag_names[f]).order_by('-id').first()
        dict_['fields'].update({FieldKind.DICT_CHOICES[v]:{'img':static(FieldKind.IMG[f]),'value':getattr(last_n,v, None),'unity':u,'tag':'{}-{}'.format(tag_names[f],f)}})

    return dict_#{'test':{'img':SensorKind.IMG[kind],'value':'valor_test', 'tag': 'tagsupe' }}
    #SensorKind.IMG
