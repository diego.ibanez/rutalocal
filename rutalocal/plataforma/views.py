from django.shortcuts import render

#from .forms import DashboardCensoForm, FiltroDashboardForm,MyGeoForm, MyBokehForm
from .forms import FiltroMapa, ZonaForm
#from puerto.models import Censo_2017_UV, Censo_2017_Manzana, Casen_2017,RegistroSensor

#from api.models import  City
#from base.enums import SensorKind
from django.forms.models import model_to_dict


from django.views.generic import TemplateView, ListView, CreateView, UpdateView
from django.urls import reverse
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import FormView
#from django.contrib.sites.models import Site
from django.db.models import Max, Min
from django.db.models import Count, F, Value, Q, CharField
#from django_pandas.io import read_frame
from bokeh.resources import CDN
import geojson

from django.db.models.functions import Cast
from django.db.models import FloatField, DecimalField

from bokeh.tile_providers import CARTODBPOSITRON, get_provider, OSM, ESRI_IMAGERY

from django.contrib.auth.mixins import UserPassesTestMixin


import numpy as np
import pandas as pd
#import folium
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.db.models import Union, Collect
from django.contrib.gis.db.models.functions import Centroid, Transform

from datetime import datetime, date
from django.core.serializers import serialize

#from .forms import censo_data
from .forms import ZonaCreateForm, OfertaTuristicaCreateForm, PuntoTuristicoCreateForm
import geopandas as gp
from shapely import wkt, wkb

from bokeh.tile_providers import get_provider, Vendors
import json
from bokeh.io import show
from bokeh.models import (CDSView, ColorBar, ColumnDataSource,
                          CustomJS, CustomJSFilter,
                          GeoJSONDataSource, HoverTool,
                          LinearColorMapper, Slider)
from bokeh.layouts import column, row, widgetbox
from bokeh.palettes import brewer
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.layouts import row,column

#from base.constants import dict_pobr_multi, dict_pobr_multi_pond, color_pobrezam,color_ambito_pobrezam
#import panel as pn
#import panel.widgets as pnw

from math import sqrt, log

from django.contrib.gis.db.models.functions import Distance, GeometryDistance
from django.contrib.gis.measure import D
from django.contrib.gis.geos import Point

from .utils import get_OSM_points, create_dataview, create_dataview_map, create_dataview_dim, get_variables_view_dict

#from base.views import BaseCreateView,BaseDetailView
#from base.utils import getPolygon

import json
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect

from .models import OfertaTuristica, Zona
from django.contrib.gis.geos import Point
from .utils import settings_leafletmap, settings_openlayers

#from django.contrib.sites.models import Site


class HomeViewV2(TemplateView):
    template_name = 'plataforma/home_v2.pug'
        #template_name = 'dashboard.pug'
#    template_name = 'visualizator/dashboard.pug'
    form_class = FiltroMapa
    def get_context_data(self, **kwargs):
      context = super(HomeViewV2, self).get_context_data(**kwargs)
      zona = self.request.POST.get('zona',None)
      
      context['zonas'] = Zona.objects.all() 
      context['zonaform'] = ZonaForm(initial={'zona':zona} if zona else {}) 
      context['settings'] = settings_openlayers(zona_id=zona)
      context['selectfeatures'] = OfertaTuristica.dict_tipo_oferta()
      #context['tile'] = settings_leafletmap(zona_id=zona)
      #context['current_site'] = Site.objects.get_current(self.request)
      #context['domain'] = context['current_site'].domain
      #print(context['settings_leaflet'])  
      return context

    def post(self, request, *args, **kwargs):
      context = self.get_context_data()
      selectedresult = request.POST.get('selectedresult',None)
      latlon = request.POST.get('latlon',None)
      lat = request.POST.get('lat',None)
      lon = request.POST.get('lon',None)
      #zona = request.POST.get('zona',None)
      #
      #if zona:
      #  context['settings_leaflet'] = settings_leafletmap(zona=zona, reinit=False)
      #  return render(request, context=context, template_name= 'plataforma/home.pug' )
      if selectedresult:
          self.template_name = 'plataforma/selectedresult.pug'
          context['result'] = OfertaTuristica.objects.filter(active=True, published=True,deleted=False, id=selectedresult).first()
          if context['result']:
            context['result'] =  context['result'].get_context_result()
      elif latlon:
          self.template_name = 'plataforma/listresults.pug'
          #print(latlon)
          latlon = latlon.split(',')
          latlon = [float(latlon[0].split('(')[-1].strip()),float(latlon[1].split(')')[0].strip())]
          #print(latlon)
          ref_location = Point(y=latlon[0],x=latlon[1],srid=4326)
          #OfertaTuristica.objects.all().
          context['list_results'] = OfertaTuristica.objects.filter(active=True, published=True,deleted=False).filter(lugar__dwithin=(ref_location, 50000)).annotate(distance=Distance('lugar', ref_location)).order_by('distance') 
          print(context['list_results'])
      return render(request, context=context, template_name= self.template_name )

class HomeView(TemplateView):
    template_name = 'plataforma/home.pug'
        #template_name = 'dashboard.pug'
#    template_name = 'visualizator/dashboard.pug'
    form_class = FiltroMapa
    def get_context_data(self, **kwargs):
      context = super(HomeView, self).get_context_data(**kwargs)
      zona = self.request.POST.get('zona',None)
      
      context['zonaform'] = ZonaForm(initial={'zona':zona} if zona else {}) 
      context['settings_leaflet'] = settings_leafletmap(zona_id=zona)
      
      #print(context['settings_leaflet'])  
      return context

    def post(self, request, *args, **kwargs):
      context = self.get_context_data()
      selectedresult = request.POST.get('selectedresult',None)
      latlon = request.POST.get('latlon',None)
      lat = request.POST.get('lat',None)
      lon = request.POST.get('lon',None)
      #zona = request.POST.get('zona',None)
      #
      #if zona:
      #  context['settings_leaflet'] = settings_leafletmap(zona=zona, reinit=False)
      #  return render(request, context=context, template_name= 'plataforma/home.pug' )
      if selectedresult:
          self.template_name = 'plataforma/selectedresult.pug'
          context['result'] = OfertaTuristica.objects.filter(active=True, published=True,deleted=False, id=selectedresult).first()
          if context['result']:
            context['result'] =  context['result'].get_context_result()
      elif latlon:
          self.template_name = 'plataforma/listresults.pug'
          #print(latlon)
          latlon = latlon.split(',')
          latlon = [float(latlon[0].split('(')[-1].strip()),float(latlon[1].split(')')[0].strip())]
          #print(latlon)
          ref_location = Point(y=latlon[0],x=latlon[1],srid=4326)
          #OfertaTuristica.objects.all().
          context['list_results'] = OfertaTuristica.objects.filter(active=True, published=True,deleted=False).filter(lugar__dwithin=(ref_location, 50000)).annotate(distance=Distance('lugar', ref_location)).order_by('distance') 
          print(context['list_results'])
      return render(request, context=context, template_name= self.template_name )
#@api_view(['POST'])
#def select_theme(request, meeting, room,format=None):
#    if request.user.is_authenticated :
#        otro =request.POST.get('otro')
#        theme =request.POST.get('theme')
#        themeRoom = FormRoom.objects.get(room_id=room)
#        themeRoom.theme=theme
#        themeRoom.otro=otro
#
#        themeRoom.save()
#        return Response({'themeRoom':themeRoom.id})
#    return Response({'themeRoom':''})

class ZonaCreateView(CreateView):
    def get(self, request, *args, **kwargs):
        context = {'form': ZonaCreateForm()}
        #print(context['form'])
        return render(request, 'plataforma/createzona.pug', context)

    def post(self, request, *args, **kwargs):
        form = ZonaCreateForm(request.POST)
        if form.is_valid():
            zona = form.save()
            zona.save()
            return HttpResponseRedirect(reverse_lazy('home'))#, args=[zona.id]))
        return render(request, 'plataforma/createzona.pug', {'form': form})

class OfertaTuristicaCreateView(CreateView):
    def get(self, request, *args, **kwargs):
        #context = {'form': OfertaTuristicaCreateForm(tipo_oferta=self.kwargs['tipo']),'tipo_oferta':self.kwargs['tipo']}
        context = {'form': PuntoTuristicoCreateForm}
        #print(context['form'])
        return render(request, 'plataforma/createoferta.pug', context)

    def post(self, request, tipo,*args, **kwargs):
        #context = super().post(request, *args, **kwargs)
        #print(tipo,'post!!')
        print(request.POST)
        #form = OfertaTuristicaCreateForm(request.POST)       
        form = PuntoTuristicoCreateForm(request.POST)
        #print(request.POST)
        if form.is_valid():
            print('holi1')
            oferta = form.save()
            oferta.save()
            return HttpResponseRedirect(reverse_lazy('home'))#, args=[zona.id]))
        else:
            print(form)
        return render(request, 'plataforma/createoferta.pug', {'form': form})#, 'tipo_oferta':self.kwargs['tipo']})


class FomularioBokehView( FormView):
    template_name = 'bokeh_geo.pug'
    success_url = '/'
#    form_class = MyBokehForm
    permission_group = 'dashboard'

    def get_context_data(self, **kwargs):
        context = super(FomularioBokehView, self).get_context_data(**kwargs)
        uvs = Censo_2017_Manzana.objects.all()
        comunas =['ANTOFAGASTA']
        if len(comunas) > 0:#comuna is not None and comuna not in ['-']:
          uvs = uvs.filter(COMUNA__in=comunas)
        #df = gpd.read_frame(uvs)
        serialization_uvs = serialize('geojson', uvs)


        geosource = GeoJSONDataSource(geojson = serialization_uvs)
        p = figure(title = 'Cantidad de personas, 2018',
           plot_height = 600 ,
           plot_width = 950,
           toolbar_location = 'below',
           tools = "pan, wheel_zoom,  reset")
        p.xgrid.grid_line_color = None
        p.ygrid.grid_line_color = None
        # Add patch renderer to figure.
        states = p.patches('xs','ys', source = geosource,
                           fill_color = None,
                           line_color = 'gray',
                           line_width = 0.25,
                           fill_alpha = 1)
        # Create hover tool
        p.add_tools(HoverTool(renderers = [states],
                              tooltips = [('Manzana','@MANZANA'),
                                        ('Population','@TOTAL_PERSONAS')]))



        script, div = components(p, CDN)
        context['script']= script
        context['div']= div
        return context
# Input GeoJSON source that contains features for plotting


# Create your views here.
def is_empty_form(c_D):
    is_blank=True
    for k, c in c_D.items():
        if type(c) is list:
            if ('-' not in c) & (len(c) >0) :
                is_blank = False
        elif type(c) is bool:
            if c == True:
                is_blank = False
        elif type(c) is str:
            if len(c) > 1:
                is_blank = False
        elif isinstance(c, datetime):
            is_blank = False
    return is_blank


def filter_procesor(filter_, not_l= ['filtrar','nombre_org','nombre_pers','contacto','asiste_kc','ambitos_mostrar']):
    c_D = filter_#.cleaned_data

    if is_empty_form(c_D):
        return {}

    query = {}
    for k, v in c_D.items():
       if k == 'user':
           print(k,v)
       if (k not in not_l) & (v is not None) & (isinstance(v, date)==False):
          if len(v) == 0 :
            v = ['-']
          query.update({k: ",".join(v)})
       else:
          if v == '':
              v = '-'
          elif v is None:
              v = '-'

          query.update({k: v})
    query.update({'table': True})
    return query

class FomularioView( FormView):
    template_name = 'form_geo.pug'
#    form_class = MyGeoForm
    permission_group = 'dashboard'
    def get_context_data(self, **kwargs):
        context = super(FomularioView, self).get_context_data(**kwargs)
        return context

class DashBoardDataView( FormView):
    #template_name = 'dashboard.pug'
    template_name = 'visualizator/dashboard.pug'


#    form_class = FiltroDashboardForm
    def get_context_data(self, **kwargs):
      context = super(DashBoardDataView, self).get_context_data(**kwargs)
      context['dash'] = 1
      if context['form'].is_bound == False:
              ### SETTING DEFAULT VIEW
                form = self.form_class({f:context['form'].fields[f].initial for f in list(context['form'].fields.keys())})
                territorio = form.data['territorio']
                dimension = form.data['dimension']
                sensor = form.data['sensor']


                p = create_dataview_map(int(territorio))
                callback = CustomJS(code="""
                getSensorVars(cb_obj.x, cb_obj.y)
                """)
                p.js_on_event('tap', callback)
                script_map, div_map = components(column(p,sizing_mode='stretch_both'), CDN)
                #script_dim, div_dim, table_dict = create_dataview_dim(int(territorio),int(dimension))
                context['script'] = script_map
                context['plot_div'] = div_map
                #context['script_dim'] = script_dim
                #context['plot_div_dim'] = div_dim
                #context['table_dim_dict'] = table_dict

      return context
    def post(self, request, *args, **kwargs):
      context = self.get_context_data()
      form = self.get_form()
      territorio = form.data['territorio']
      dimension = form.data['dimension']

      ## if is clicked censodata
      try:
        censo_data = form.data['censodata']
      except:
        censo_data = False

      #script_map, div_map = create_dataview_map(int(territorio),censo_data)

      p = create_dataview_map(int(territorio),censo_data)
      callback = CustomJS(code="""
      getSensorVars(cb_obj.x, cb_obj.y)
      """)
      p.js_on_event('tap', callback)
      script_map, div_map = components(column(p,sizing_mode='stretch_both'), CDN)


      script_dim, div_dim, table_dict = create_dataview_dim(int(territorio),int(dimension))

      context['script'] = script_map
      context['plot_div'] = div_map
      context['script_dim'] = script_dim
      context['plot_div_dim'] = div_dim
      context['table_dim_dict'] = table_dict

      return render(request, context=context, template_name= self.template_name )

class SensorVarsView(TemplateView):
    template_name = 'visualizator/sensor_vars.pug'
    def get_context_data(self, **kwargs):
        context = super(SensorVarsView, self).get_context_data(**kwargs)
        context['dash'] = 1
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        x = request.GET.get('x', None)
        y = request.GET.get('y', None)
        geo = getPolygon(x,y)
        pol = GEOSGeometry(geo)
        #.annotate(geometry_m=Transform('locacion',3857))
        sensor = RegistroSensor.objects.filter(locacion__intersects=pol)#.first()
        if len(sensor) > 0:
            context['sensor_vars']= get_variables_view_dict(sensor)['fields']
        else:
            context['sensor_vars'] = {} #k:n for k,n in }

        return render(request, context=context, template_name= self.template_name)


class SensorHeaderView(TemplateView):
    template_name = 'visualizator/sensor_header.pug'
    def get_context_data(self, **kwargs):
        context = super(SensorHeaderView, self).get_context_data(**kwargs)
        context['dash'] = 1
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        x = request.GET.get('x', None)
        y = request.GET.get('y', None)
        geo = getPolygon(x,y)
        pol = GEOSGeometry(geo)

        #.annotate(geometry_m=Transform('locacion',3857))
        sensor = RegistroSensor.objects.filter(locacion__intersects=pol)#.first()

        if len(sensor) > 0:
#            print(sensor)
            context['sensor_header']= get_variables_view_dict(sensor)['sensor']
        else:
            #sensor= RegistroSensor.objects.all().first()

            context['sensor_header'] = {} #k:n for k,n in }

        return render(request, context=context, template_name= self.template_name)


class SensorDimHistoryView(TemplateView):
    template_name = 'visualizator/sensor_dim_history.pug'
    def get_context_data(self, **kwargs):
        context = super(SensorDimHistoryView, self).get_context_data(**kwargs)
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        tag = request.GET.get('tag', None)
#        sensor_name = tag.split('-')[0]
#        sensor_dim = tag.split('-')[1]
        script, div = City.getTimeSeriaSingleGraph(tag)
        context['plot_history'] = div
        context['script_history'] = script
        context['dash'] = 1
        #sensor = RegistroSensor.objects.filter(locacion__intersects=pol).first()
        #
        #if sensor is not None:

        #    context['sensor_vars']= get_variables_view_dict(sensor)
        #else:
        #    #sensor= RegistroSensor.objects.all().first()

        #    context['sensor_vars'] = {} #k:n for k,n in }
        return render(request, context=context, template_name= self.template_name )




class DataMapView(TemplateView):
    template_name = 'visualizator/data_map.pug'
    def get_context_data(self, **kwargs):
        context = super(DataMapView, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
      context = self.get_context_data()
      #form = self.get_form()

      form = FiltroDashboardForm(request.POST)
      if form.is_valid():
          data = form.cleaned_data

          territorio = data['territorio']
          ## if is clicked censodata
          try:
            censo_data = data['censodata']
          except:
            censo_data = False

          #script_map, div_map = create_dataview_map(int(territorio),censo_data)

          p = create_dataview_map(int(territorio),censo_data)
          callback = CustomJS(code="""
          getSensorVars(cb_obj.x, cb_obj.y)
          """)
          p.js_on_event('tap', callback)
          script_map, div_map = components(column(p,sizing_mode='stretch_both'), CDN)
          context['script'] = script_map
          context['plot_div'] = div_map
          return render(request, context=context, template_name= self.template_name )

class DataDimensionsView(TemplateView):
    template_name = 'visualizator/data_dimensions.pug'
    def get_context_data(self, **kwargs):
        context = super(DataDimensionsView, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
      context = self.get_context_data()
      form = FiltroDashboardForm(request.POST)
      if form.is_valid():
          data = form.cleaned_data

          territorio = data['territorio']
          dimension = data['dimension']

          script_dim, div_dim, table_dict = create_dataview_dim(int(territorio),int(dimension))

          context['script'] = script_dim
          context['plot_div'] = div_dim
          context['table_dim_dict'] = table_dict

          return render(request, context=context, template_name= self.template_name )


