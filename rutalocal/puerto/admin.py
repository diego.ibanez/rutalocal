from django.contrib import admin
from django.contrib.gis import admin as admingeo

# Register your models here.
from .models import  RegistroSensor, Censo_2017_Manzana, Censo_2017_UV, Casen_2017, BitacoraPuerto

from .models import Limite_Urbano_Censal_SIEDU,Distrito_SIEDU,Comuna_SIEDU,EDUCACION_BASICA,EJES_VIALES,EDUCACION_INICIAL,CENTROS_SALUD,PARADEROS_TRANSPORTE,PARQUES,PLAZAS,CICLOVIAS,BPU20_Manzana_Dist_Ind,BPU1_Manzana_Dist_Ind,BPU22_Manzana_Dist_Ind,BPU7_Manzana_Dist_Ind,BPU25_Manzana_Dist_Ind,EA48_Exposicion_Tsunami,IS40_Calidad_Veredas,EA31_CCU_2017,EA31_CCU_2011,IS39_Unidades_Vecinales_Segregacion,BPU3_Manzana_Por_Pob,BPU3_Manzana_Dist_Ind,Zona_SIEDU,BPU17_Ejes_Urbano_Rural,CLASIFICACION_SUELO_I_II_III,SITIOS_ESTRATEGIAS_REGIONALES,CONCESIONES_SANITARIAS_2018,RUIDO_DIURNO,RUIDO_NOCTURNO,DE25_Ejes_Principales,DE25_Nodos,DE25_Red_Conteo_Nodos,IP33A_CCU2017_por_Comuna,IP33B_Superficie_Suelo_Agricola,IP33C_Superficie_Sitios_Prioritarios,EA8_9_Concesiones_Sanitarias

from .models import IndicadoresSIEDU, MetadatosIndicadoresSIEDU

#from mapbox_location_field.spatial.admin import SpatialMapAdmin

#admin.site.register(RegistroSensor, SpatialMapAdmin)


admin.site.register(BitacoraPuerto)
admingeo.site.register(Censo_2017_Manzana, admingeo.GeoModelAdmin)
admingeo.site.register(Censo_2017_UV, admingeo.GeoModelAdmin)

#admingeo.site.register(Limite_Urbano_Censal_SIEDU, admingeo.GeoModelAdmin)
admingeo.site.register(Casen_2017, admingeo.GeoModelAdmin)

admingeo.site.register(Limite_Urbano_Censal_SIEDU, admingeo.GeoModelAdmin)
admingeo.site.register(Distrito_SIEDU, admingeo.GeoModelAdmin)
admingeo.site.register(Comuna_SIEDU, admingeo.GeoModelAdmin)
admingeo.site.register(EDUCACION_BASICA, admingeo.GeoModelAdmin)
admingeo.site.register(EJES_VIALES, admingeo.GeoModelAdmin)
admingeo.site.register(EDUCACION_INICIAL, admingeo.GeoModelAdmin)
admingeo.site.register(CENTROS_SALUD, admingeo.GeoModelAdmin)
admingeo.site.register(PARADEROS_TRANSPORTE, admingeo.GeoModelAdmin)
admingeo.site.register(PARQUES, admingeo.GeoModelAdmin)
admingeo.site.register(PLAZAS, admingeo.GeoModelAdmin)
admingeo.site.register(CICLOVIAS, admingeo.GeoModelAdmin)
admingeo.site.register(BPU20_Manzana_Dist_Ind, admingeo.GeoModelAdmin)
admingeo.site.register(BPU1_Manzana_Dist_Ind, admingeo.GeoModelAdmin)
admingeo.site.register(BPU22_Manzana_Dist_Ind, admingeo.GeoModelAdmin)
admingeo.site.register(BPU7_Manzana_Dist_Ind, admingeo.GeoModelAdmin)
admingeo.site.register(BPU25_Manzana_Dist_Ind, admingeo.GeoModelAdmin)
admingeo.site.register(EA48_Exposicion_Tsunami, admingeo.GeoModelAdmin)
admingeo.site.register(IS40_Calidad_Veredas, admingeo.GeoModelAdmin)
admingeo.site.register(EA31_CCU_2017, admingeo.GeoModelAdmin)
admingeo.site.register(EA31_CCU_2011, admingeo.GeoModelAdmin)
admingeo.site.register(IS39_Unidades_Vecinales_Segregacion, admingeo.GeoModelAdmin)
admingeo.site.register(BPU3_Manzana_Por_Pob, admingeo.GeoModelAdmin)
admingeo.site.register(BPU3_Manzana_Dist_Ind, admingeo.GeoModelAdmin)
admingeo.site.register(Zona_SIEDU, admingeo.GeoModelAdmin)
admingeo.site.register(BPU17_Ejes_Urbano_Rural, admingeo.GeoModelAdmin)
admingeo.site.register(CLASIFICACION_SUELO_I_II_III, admingeo.GeoModelAdmin)
admingeo.site.register(SITIOS_ESTRATEGIAS_REGIONALES, admingeo.GeoModelAdmin)
admingeo.site.register(CONCESIONES_SANITARIAS_2018, admingeo.GeoModelAdmin)
admingeo.site.register(RUIDO_DIURNO, admingeo.GeoModelAdmin)
admingeo.site.register(RUIDO_NOCTURNO, admingeo.GeoModelAdmin)
admingeo.site.register(DE25_Ejes_Principales, admingeo.GeoModelAdmin)
admingeo.site.register(DE25_Nodos, admingeo.GeoModelAdmin)
admingeo.site.register(DE25_Red_Conteo_Nodos, admingeo.GeoModelAdmin)
admingeo.site.register(IP33A_CCU2017_por_Comuna, admingeo.GeoModelAdmin)
admingeo.site.register(IP33B_Superficie_Suelo_Agricola, admingeo.GeoModelAdmin)
admingeo.site.register(IP33C_Superficie_Sitios_Prioritarios, admingeo.GeoModelAdmin)
admingeo.site.register(EA8_9_Concesiones_Sanitarias, admingeo.GeoModelAdmin)


admin.site.register(IndicadoresSIEDU)
admin.site.register(MetadatosIndicadoresSIEDU)