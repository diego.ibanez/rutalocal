from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from django.contrib.auth.views import LogoutView
from django.conf import settings
from django.conf.urls.static import static


from puerto import views


urlpatterns = [
    path('censo/', views.PuertoCenso.as_view(), name='puerto_censo'),
    path('casen/', views.PuertoCasen.as_view(), name='puerto_casen'),
    path('siedu/', views.PuertoSIEDU.as_view(), name='puerto_siedu'),
    path('registro_sensor/', views.RegistroSensorView.as_view(), name='registro_sensor'),
    path('', views.Puerto.as_view(), name='puerto'),

]
