from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView, ListView, CreateView, UpdateView
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.decorators import user_passes_test

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# Create your views here.
from background_task import background
import requests

from .models import Censo_2017_Manzana, Censo_2017_UV, BitacoraPuerto, RegistroSensor, Casen_2017, IndicadoresSIEDU, MetadatosIndicadoresSIEDU
from .forms import RegistroSensorForm

import inspect
import os

import importlib
from django.contrib.gis.geos import Polygon, MultiPolygon

import wget
import tempfile
import geojson

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import get_user_model

import pandas as pd
import numpy as np
from glob import glob

import patoolib

from django.apps import apps
import fiona
import geopandas as gpd
from django.contrib.gis.geos import GEOSGeometry
import unidecode


def sende_mail_to_admins(dict_):
    subject= 'Importación de datos CENSO - Plataforma Plenario - CINNDA'

    #text = 'Hola!<br>Agregaste una nueva iniciativa, para confirmarla haz click en https://{}/congresoabierto/confirm/?token={}'.format(current_site, form.instance.token)
    #resp=send_mail(subject=subject, message= subject,from_email='congresoabierto@karolcariola.cl', recipient_list=[form.instance.email], html_message=text,  fail_silently=False)

    pass


def create_string_model(list_fields):
    string = "\nclass Censo(models.Model):"
    for f in list_fields:

        if any([s in f['type'] for s in ['String','Varchar']]):
            tipo = 'CharField'
            extras= 'max_length={}'.format(f["length"])
        elif any([s in f['type'] for s in ['Integer','OID']]):
            tipo = 'IntegerField'
            extras = ''
        elif 'Double' in f['type']:
            tipo='DecimalField'
            extras = 'max_digits=25, decimal_places=19'
        string += "\n   {} = models.{}(verbose_name='{}',{})".format(f['name'].replace('__','_'),tipo,f['alias'],extras)
    string += "\n   geometry = models.PolygonField()"
    return string

def download_file(url, path):
    local_filename = path
    # NOTE the stream=True parameter below
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for n, chunk in enumerate(r.iter_content(chunk_size=8192)):
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                #if chunk:
                f.write(chunk)
                if n % 100:
                    print('..')
    return local_filename


def transfor_label(i):
    r = i.strip().replace("°","").replace("*","x").replace(" / "," ").replace(" ","_")
    unaccented_string = unidecode.unidecode(r)
    return unaccented_string

def drop_full_nan_col(df):
    c = [c for c in df.columns.tolist() if df[c].isnull().sum() == df.shape[0]]
    return df.drop(c,axis=1)

@background()
def portar_siedu_cartografia(user):
    query = 'http://siedu.ine.cl/descargar/CARTOGRAFIA.rar'
    print(query)

    temp = tempfile.TemporaryFile()
    temp.close()
    temp = tempfile.TemporaryFile()

    temp_dir = tempfile.gettempdir()
    path = os.path.join(str(temp_dir),str(temp.name))
    #path = "/tmp/20"
    #print(path)
    #wget.download(query,path)

    path_to_extract_pat = os.path.join(str(temp_dir),'20200319_CARTOGRAFIA_SIEDU.gdb')
    #path_to_extract_pat = '/home/diego/nestolab/data/CARTOGRAFIA/20200319_CARTOGRAFIA_SIEDU.gdb'

    if not os.path.exists(path_to_extract_pat):
        r = download_file(query,path)
    #print(path)
        patoolib.extract_archive(path, outdir=str(temp_dir))
        patoolib.extract_archive(os.path.join(temp_dir,'CARTOGRAFIA','20200319_CARTOGRAFIA_SIEDU.rar'), outdir=str(temp_dir))

    #path_to_extract_pat = os.path.join(str(temp_dir),'20200319_CARTOGRAFIA_SIEDU.gdb')
    path_extracted = path_to_extract_pat#glob(path_to_extract_pat)[0]


    layers_list = fiona.listlayers(path_extracted)

    for l in layers_list:
        print(l)
        gdf = gpd.read_file(path_extracted, driver='FileGDB', layer=l)

        model = apps.get_model('puerto', l)

        bitacora = {}
        bitacora["initial_n_model"] = model.objects.all().count()


        ### extract .dta STATA

        bitacora["n_to_save"] = gdf.shape[0]
        n_saves = 0


        ## FIXING COLS WITH '__' or finished in '_'
        new_cols = [c.replace('__','_') if c[-1] != '_' else c[:-1].replace('__','_') for c in list(gdf.columns)]

        gdf = gdf.rename(columns=dict(zip(list(gdf.columns),new_cols)))
        ##

        for n, rows in gdf.iterrows():
            try:
                #print("ASDASDASDASDASD")
                r_aux = rows.to_dict()
                geo = r_aux.pop('geometry')
                #print(r_aux['geometry'])
                #print(type(geo))
                #print(geo.__dict__)
                geo_ = GEOSGeometry(str(geo))
                #print(type(geo_))

                r_aux.update({'geometry': geo_})
                #print(r_aux['geometry'].type)
                model_to_save = model.objects.create(**r_aux)
                #model_to_save.save()
                n_saves += 1
            except:
                continue
        bitacora["n_saved"] = n_saves
        bitacora["final_n_model"] = model.objects.all().count()

        bitacora["model"] = l

        bitacora["user"] = get_user_model().objects.all().get(username=user)
        ## saving bitacora
        bitacora_model = BitacoraPuerto(**bitacora)
        bitacora_model.save()

        message = bitacora

        sende_mail_to_admins(message)
        ##
        print('{} SIEDU ported!'.format(l))
    temp.close()
        #pass

@background()
def portar_siedu_indicadores(user):
    query = 'http://siedu.ine.cl/descargar/INDICADORES.rar'
    print(query)

    temp = tempfile.TemporaryFile()
    temp.close()
    temp = tempfile.TemporaryFile()

    temp_dir = tempfile.gettempdir()
    path = os.path.join(str(temp_dir),str(temp.name))
    #path = "/tmp/20"
    #print(path)
    #wget.download(query,path)

    path_to_extract_pat = os.path.join(str(temp_dir),'INDICADORES','LB2018_MATRIZ_SIEDU.xlsx')
    #path_to_extract_pat = '/home/diego/nestolab/data/CARTOGRAFIA/20200319_CARTOGRAFIA_SIEDU.gdb'

    if not os.path.exists(path_to_extract_pat):
        r = download_file(query,path)
    #print(path)
        patoolib.extract_archive(path, outdir=str(temp_dir))
        #patoolib.extract_archive(os.path.join(temp_dir,'CARTOGRAFIA','20200319_CARTOGRAFIA_SIEDU.rar'), outdir=str(temp_dir))

    #path_to_extract_pat = os.path.join(str(temp_dir),'20200319_CARTOGRAFIA_SIEDU.gdb')
    path_extracted = path_to_extract_pat#glob(path_to_extract_pat)[0]



    ## loading meta data dataframe
    df_meta = pd.read_excel(path_extracted, sheet_name='Matriz_Metadata',header=1)

    ## rename final
    dict_cols_meta = {c:transfor_label(c) for c in df_meta.columns.tolist()}
    dict_labels_meta = dict(zip(dict_cols_meta.values(),dict_cols_meta.keys()))
    df_meta_final = df_meta.rename(columns=dict_cols_meta)

    ## load header data
    df_header = pd.read_excel(path_extracted, sheet_name='Matriz_Indicadores',header=2,nrows=1)
    ## rename header cols
    dict_cols_header = {c:transfor_label(c) for c in df_header.columns.tolist()}
    df_header = df_header.rename(columns=dict_cols_header)

    ## saving columns list
    cols = df_header.columns.tolist()

    ## creating dict of labels for indicator
    d_labels = df_header.T.to_dict()[0]
    to_pop = []
    for k, v in d_labels.items():
        if 'Unnamed' in k:
            to_pop.append(k)
    for p in to_pop:
        d_labels.pop(p)

    d_labels.update(dict_labels_meta)

    ## load comuna data
    df_com = pd.read_excel(path_extracted, sheet_name='Matriz_Indicadores',header=7, nrows=117)

    ## create dict to rename columns of comuna
    dr_com = {}
    for n, c in enumerate(df_com.columns.tolist()):
        if n == 6:
            dr_com.update({c:'CUT'})
        elif n > 6:
            dr_com.update({c:cols[n]})

    ## rename columns of comuna data
    df_com = df_com.rename(columns=dr_com)
    ## drop full nan cols
    df_com = drop_full_nan_col(df_com)

    ## load ciudad data

    df_ciu = pd.read_excel(path_extracted, sheet_name='Matriz_Indicadores',header=134, nrows=35)

    ## create dict to rename columns of ciudad
    dr_ciu = {}
    for n, c in enumerate(df_ciu.columns.tolist()):
        if n > 6:
            dr_ciu.update({c:cols[n]})

    ## rename columns of ciudad data
    df_ciu = df_ciu.rename(columns=dr_ciu)
    ## drop full nan cols
    df_ciu = drop_full_nan_col(df_ciu)
    ## drop ['Región','Área','Nombre Ciudad']
    df_ciu = df_ciu.drop(['Región','Área','Nombre Ciudad'],axis=1)

    df_full = df_com.merge(df_ciu, on='Cod_Ciudad',how='left')
    df_full.replace('S/I',np.nan, inplace=True)
    d_rename_full = {'Región':'Region', 'Área':'Area', 'Nombre Ciudad':'Nombre_Ciudad'}
    df_full = df_full.rename(columns=d_rename_full)
    ## rename final
    dict_cols_full = {c:transfor_label(c) for c in df_full.columns.tolist()}
    df_full = df_full.rename(columns=dict_cols_full)

    dfs_dict={
    'MetadatosIndicadoresSIEDU':df_meta_final,
    'IndicadoresSIEDU':df_full
    }
    for m, df in dfs_dict.items():

        model = apps.get_model('puerto', m)

        bitacora = {}
        bitacora["initial_n_model"] = model.objects.all().count()


        ### extract .dta STATA

        bitacora["n_to_save"] = df.shape[0]
        n_saves = 0


        for n, rows in df.iterrows():
            try:
                r_aux = rows.to_dict()

                model_to_save = model.objects.create(**r_aux)

                n_saves += 1
            except:
                continue
        bitacora["n_saved"] = n_saves
        bitacora["final_n_model"] = model.objects.all().count()

        bitacora["model"] = m

        bitacora["user"] = get_user_model().objects.all().get(username=user)
        ## saving bitacora
        bitacora_model = BitacoraPuerto(**bitacora)
        bitacora_model.save()

        message = bitacora

        sende_mail_to_admins(message)
        ##
        print('{} SIEDU ported!'.format(m))
    temp.close()
        #pass



@background()
def portar_censo_2017(user, granularidad='UV'):
    #query = "https://services5.arcgis.com/hUyD8u3TeZLKPe4T/arcgis/rest/services/Cifras_Comunas_2017/FeatureServer/0/query?where=1%3D1&outFields=*&outSR=4326&f=json"
    #query= "https://sig.ine.cl/server/rest/services/Open_Data/Censos/MapServer/5/query?where=1%3D1&outFields=*&outSR=4326&f=json"
    ###
    bitacora = {}
    if granularidad == 'UV':
            query = "https://opendata.arcgis.com/datasets/a90cf11da90f4d0c86934b8636fd9779_5.geojson"
            bitacora["model"] = "Censo_2017_UV"
            modelo_censo = Censo_2017_UV

    elif granularidad == 'Manzana':
            query = 'https://opendata.arcgis.com/datasets/54e0c40680054efaabeb9d53b09e1e7a_0.geojson'
            bitacora["model"] = "Censo_2017_Manzana"
            modelo_censo = Censo_2017_Manzana
    print(query)
    #print(settings.AUTH_USER_MODEL)
    #print(settings.AUTH_USER_MODEL.objects)
    print(get_user_model().objects.all().get(username=user))
    #response = requests.get(query)
    #
    #if response.status_code == 200:
    #   dic_response = response.json()
    #   print("exceededTransferLimit: ",dic_response['exceededTransferLimit'])
    #try:
    #    censo = models_all.Censo()
    #    print('not')
    #except AttributeError:
    #    ## create model
    #    print("..")
    #    string_model = create_string_model(dic_response["fields"])
    #    with open("puerto/models.py", "a") as f:
    #        f.write(string_model)
    #    print(string_model)
    #
    #    ## reload system making changes in database
    #    os.system('python manage.py makemigrations')
    #    os.system('python manage.py migrate')
    #
    #    ## reload models
    #    importlib.reload(models_all)
    #    censo = models_all.Censo()

    ## save data
    #censo = models_all.Censo()
    temp = tempfile.TemporaryFile()
    temp.close()
    temp = tempfile.TemporaryFile()

    temp_dir = tempfile.gettempdir()
    path = os.path.join(str(temp_dir),str(temp.name))

    r = download_file(query,path)

#    path = '/home/diego/Descargas/Microdatos_Censo_2017%3A_Manzana.geojson'

    #print(path)
    bitacora["initial_n_model"] = modelo_censo.objects.all().count()
    with open(path) as f:
        gj = geojson.load(f)
        bitacora["n_to_save"] = len(gj['features'])
        n_saves = 0
        for features in gj['features']:

            if features['geometry']['type'] == 'MultiPolygon':
                polys = [p[0] for p in features['geometry']['coordinates']]
            else:
                polys = features['geometry']['coordinates']


            try:
            #print(features['properties'].keys())
                censo = modelo_censo.objects.create(**features['properties'],geometry= MultiPolygon([Polygon(p) for p in polys]))
                #censo.save()
                n_saves += 1
            except:
                continue

        bitacora["n_saved"] = n_saves

    temp.close()

    bitacora["final_n_model"] = modelo_censo.objects.all().count()
    bitacora["user"] = get_user_model().objects.all().get(username=user)
    ## saving bitacora
    bitacora_model = BitacoraPuerto(**bitacora)
    bitacora_model.save()


    message = bitacora


    sende_mail_to_admins(message)
    ##
    print('Censo 2017 {} ported!'.format(granularidad))
    #pass

@background()
def portar_casen_2017(user):
    """Portar CASEN 2017"""

    query = "http://observatorio.ministeriodesarrollosocial.gob.cl/storage/docs/casen/2017/casen_2017_stata.rar"
    print(query)

    temp = tempfile.TemporaryFile()
    temp.close()
    temp = tempfile.TemporaryFile()

    temp_dir = tempfile.gettempdir()
    path = os.path.join(str(temp_dir),str(temp.name))


    path_to_extract_pat = os.path.join(str(temp_dir),'Casen 2017.dta')
    #path_to_extract_pat = '/home/diego/Descargas/Casen 2017.dta'
    if not os.path.exists(path_to_extract_pat):
        r = download_file(query,path)
        patoolib.extract_archive(path, outdir=str(temp_dir))

    bitacora = {}

    ### extract .dta STATA
    #path_extracted = glob(path_to_extract_pat)[0]
    #print(path_extracted)

    path_extracted = path_to_extract_pat#'/home/diego/Descargas/Casen 2017.dta'
    bitacora["initial_n_model"] = Casen_2017.objects.all().count()

    #with open(path_extracted) as f:
    reader = pd.read_stata(path_extracted, convert_categoricals=False,chunksize=10000)
    #gj = pd.DataFrame()
    n_to_save = 0
    n_saves = 0

    for n_r, itm in enumerate(reader):
        #gj=gj.append(itm)
        n_to_save += itm.shape[0]

        ## TO COMPLETE SOME INCOMPLETE IMPORT
        #if n_to_save < 195000:
        #    continue

    #print(gj.shape)

    #bitacora["n_to_save"] = gj.shape[0]

    #n_saves = 0
        if n_r % 10 == 0:
            print(n_r)
        for n, rows in itm.iterrows():
            try:
                casen = Casen_2017.objects.create(**rows.to_dict())
                #casen.save()
                n_saves += 1
            except:
                continue
    bitacora["n_to_save"] = n_to_save
    bitacora["n_saved"] = n_saves
    temp.close()

    bitacora["final_n_model"] = Casen_2017.objects.all().count()

    bitacora["model"] = "Casen_2017"

    bitacora["user"] = get_user_model().objects.all().get(username=user)
    ## saving bitacora
    bitacora_model = BitacoraPuerto(**bitacora)
    bitacora_model.save()

    message = bitacora

    sende_mail_to_admins(message)
    ##
    print('Casen_2017 ported!')
    #pass


class PuertoSIEDU(TemplateView):
    template_name = 'port_siedu_data.pug'
    permission_group = 'puerto'

    def post(self, request, *args, **kwargs):
        if "siedu-cartografia" in request.POST.keys():
            print('post')
            portar_siedu_cartografia(request.user.username)
            messages.success(self.request, 'Inició el proceso de carga de la información de SIEDU CARTOGRAFIA INE. Los administradores recibirán un correo cuando finalice.')

        if "siedu-indicadores" in request.POST.keys():
            print('post')
            portar_siedu_indicadores(request.user.username)
            messages.success(self.request, 'Inició el proceso de carga de la información de SIEDU INDICADORES INE. Los administradores recibirán un correo cuando finalice.')

        return render(request,template_name="port.pug")

    @method_decorator(login_required(login_url='/admin/login/'))
    def dispatch(self, *args, **kwargs):
        return super(PuertoSIEDU, self).dispatch(*args, **kwargs)


class PuertoCenso(TemplateView):
    template_name = 'port_censo_data.pug'
    permission_group = 'puerto'

    def post(self, request, *args, **kwargs):
        if "censo_2017" in request.POST.keys():
            print('post')

            #portar_censo_2017(request.user.username, granularidad = 'UV')

            portar_censo_2017(request.user.username, granularidad = 'Manzana')

            messages.success(self.request, 'Inició el proceso de carga de la información del Censo 2017. Los administradores recibirán un correo cuando finalice.')

        return render(request,template_name="port.pug")

    @method_decorator(login_required(login_url='/admin/login/'))
    def dispatch(self, *args, **kwargs):
        return super(PuertoCenso, self).dispatch(*args, **kwargs)


class PuertoCasen(TemplateView):
    template_name = 'port_casen_data.pug'
    permission_group = 'puerto'

    def post(self, request, *args, **kwargs):
        if "casen_2017" in request.POST.keys():
            print('post')

            portar_casen_2017(request.user.username)
            messages.success(self.request, 'Inició el proceso de carga de la información de CASEN 2017. Los administradores recibirán un correo cuando finalice.')

        return render(request,template_name="port.pug")

    @method_decorator(login_required(login_url='/admin/login/'))
    def dispatch(self, *args, **kwargs):
        return super(PuertoCasen, self).dispatch(*args, **kwargs)



class Puerto( TemplateView):
    template_name = 'port.pug'
    permission_group = 'puerto'



class RegistroSensorView( CreateView):
    template_name = 'sensor_register.pug'
    permission_group = 'puerto'
    form_class = RegistroSensorForm
    success_url = '../../puerto'

    def form_valid(self, form):
        messages.success(self.request, 'Haz registrado un nuevo Sensor.')
        return super(RegistroSensorView, self).form_valid(form)

    @method_decorator(login_required(login_url='/admin/login/'))
    def dispatch(self, *args, **kwargs):
        return super(RegistroSensorView, self).dispatch(*args, **kwargs)
