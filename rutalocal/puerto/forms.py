from django import forms
from .models import RegistroSensor
from django_summernote.fields import  SummernoteWidget

from django.contrib.gis import forms as formsgeo

#class PortarCensoForm(forms.Form):
#    content = forms.CharField()

class RegistroSensorForm(forms.ModelForm):

    direccion = forms.CharField(
            widget=forms.HiddenInput,
            )

    class Meta:
   		model = RegistroSensor
   		fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(RegistroSensorForm, self).__init__(*args, **kwargs)
        self.fields['direccion'].required = False
        self.fields['locacion'].widget.attrs['style'] = 'border-width: 0px'
